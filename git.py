import http.server
import socketserver
import json
from threadw import Threaded

# FIXME: Make this a list and make GitServ join this channel (Not hardcoded in irc.py:add_users())
GIT_CHANNEL = '#figserv'

def make_handler_class(irc):
	class Handler(http.server.SimpleHTTPRequestHandler):
		def do_POST(self):
			# FIXME: Send 403 if they're not coming from an IP we like or if the secret is incorrect
			self.send_response(200)
			self.end_headers()
			self.wfile.write(b'\n')

			content_len = int(self.headers.get_all('content-length', failobj=0)[0])
			post_body = self.rfile.read(content_len)
			#print(json.dumps(json.loads(post_body.decode("UTF-8", "replace")), sort_keys=True, indent=4))
			body = json.loads(post_body.decode("UTF-8", "replace"))
			serv = irc.service_users['GitServ']

			message = []

			# TODO: Say who's what was acted upon (bigfoot547 closed luk3yx's issue)
			# FIXME: Workaround for oversight in gitlab (incorrect node name). Someone report this issue please ;)
			if 'event_type' in body:
				if body['object_kind'] == 'issue' and body['event_type'] == 'issue':
					if body['object_attributes']['action'] == 'open':
						if body['user']['name'] == body['user']['username']:
							message.append("[{}7{}/{}\3] \2{}\2 opened an issue: \2{}\2 (Issue \2#{}\2) | \2Link\2: {}".format("\3", body['project']['namespace'], body['project']['name'], body['user']['username'], body['object_attributes']['title'], body['object_attributes']['iid'], body['object_attributes']['url']))
						else:
							message.append("[{}7{}/{}\3] \2{}\2 \35({})\35 opened an issue: \2{}\2 (Issue \2#{}\2) | \2Link\2: {}".format("\3", body['project']['namespace'], body['project']['name'], body['user']['name'], body['user']['username'], body['object_attributes']['title'], body['object_attributes']['iid'], body['object_attributes']['url']))
					elif body['object_attributes']['action'] == 'close':
						if body['user']['name'] == body['user']['username']:
							message.append("[{}7{}/{}\3] \2{}\2 opened an issue: \2{}\2 (Issue \2#{}\2) | \2Link\2: {}".format("\3", body['project']['namespace'], body['project']['name'], body['user']['username'], body['object_attributes']['title'], body['object_attributes']['iid'], body['object_attributes']['url']))
						else:
							message.append("[{}7{}/{}\3] \2{}\2 \35({})\35 closed an issue: \2{}\2 (Issue \2#{}\2) | \2Link\2: {}".format("\3", body['project']['namespace'], body['project']['name'], body['user']['name'], body['user']['username'], body['object_attributes']['title'], body['object_attributes']['iid'], body['object_attributes']['url']))
					elif body['object_attributes']['action'] == 'reopen':
						if body['user']['name'] == body['user']['username']:
							message.append("[{}7{}/{}\3] \2{}\2 reopened an issue: \2{}\2 (Issue \2#{}\2) | \2Link\2: {}".format("\3", body['project']['namespace'], body['project']['name'], body['user']['username'], body['object_attributes']['title'], body['object_attributes']['iid'], body['object_attributes']['url']))
						else:
							message.append("[{}7{}/{}\3] \2{}\2 \35({})\35 reopened an issue: \2{}\2 (Issue \2#{}\2) | \2Link\2: {}".format("\3", body['project']['namespace'], body['project']['name'], body['user']['name'], body['user']['username'], body['object_attributes']['title'], body['object_attributes']['iid'], body['object_attributes']['url']))
				elif body['object_kind'] == 'merge_request' and body['event_type'] == 'merge_request':
					if body['object_attributes']['action'] == 'open':
						if body['user']['name'] == body['user']['username']:
							message.append("[{}7{}/{}\3] \2{}\2 opened a merge request: \2{}\2 (MR \2#{}\2) | Merging \2{}:{}\2 into \2{}:{}\2 | \2Link\2: {}".format("\3", body['project']['namespace'], body['project']['name'], body['user']['username'], body['object_attributes']['title'], body['object_attributes']['iid'], body['object_attributes']['source']['namespace'], body['object_attributes']['source_branch'], body['object_attributes']['target']['namespace'], body['object_attributes']['target_branch'], body['object_attributes']['url']))
						else:
							message.append("[{}7{}/{}\3] \2{}\2 \35({})\35 opened a merge request: \2{}\2 (MR \2#{}\2) | Merging \2{}:{}\2 into \2{}:{}\2 | \2Link\2: {}".format("\3", body['project']['namespace'], body['project']['name'], body['user']['name'], body['user']['username'], body['object_attributes']['title'], body['object_attributes']['iid'], body['object_attributes']['source']['namespace'], body['object_attributes']['source_branch'], body['object_attributes']['target']['namespace'], body['object_attributes']['target_branch'], body['object_attributes']['url']))
					elif body['object_attributes']['action'] == 'close':
						if body['user']['name'] == body['user']['username']:
							message.append("[{}7{}/{}\3] \2{}\2 closed a merge request: \2{}\2 (MR \2#{}\2) | Merging \2{}:{}\2 into \2{}:{}\2 | \2Link\2: {}".format("\3", body['project']['namespace'], body['project']['name'], body['user']['username'], body['object_attributes']['title'], body['object_attributes']['iid'], body['object_attributes']['source']['namespace'], body['object_attributes']['source_branch'], body['object_attributes']['target']['namespace'], body['object_attributes']['target_branch'], body['object_attributes']['url']))
						else:
							message.append("[{}7{}/{}\3] \2{}\2 \35({})\35 closed a merge request: \2{}\2 (MR \2#{}\2) | Merging \2{}:{}\2 into \2{}:{}\2 | \2Link\2: {}".format("\3", body['project']['namespace'], body['project']['name'], body['user']['name'], body['user']['username'], body['object_attributes']['title'], body['object_attributes']['iid'], body['object_attributes']['source']['namespace'], body['object_attributes']['source_branch'], body['object_attributes']['target']['namespace'], body['object_attributes']['target_branch'], body['object_attributes']['url']))
					elif body['object_attributes']['action'] == 'reopen':
						if body['user']['name'] == body['user']['username']:
							message.append("[{}7{}/{}\3] \2{}\2 reopened a merge request: \2{}\2 (MR \2#{}\2) | Merging \2{}:{}\2 into \2{}:{}\2 | \2Link\2: {}".format("\3", body['project']['namespace'], body['project']['name'], body['user']['username'], body['object_attributes']['title'], body['object_attributes']['iid'], body['object_attributes']['source']['namespace'], body['object_attributes']['source_branch'], body['object_attributes']['target']['namespace'], body['object_attributes']['target_branch'], body['object_attributes']['url']))
						else:
							message.append("[{}7{}/{}\3] \2{}\2 \35({})\35 reopened a merge request: \2{}\2 (MR \2#{}\2) | Merging \2{}:{}\2 into \2{}:{}\2 | \2Link\2: {}".format("\3", body['project']['namespace'], body['project']['name'], body['user']['name'], body['user']['username'], body['object_attributes']['title'], body['object_attributes']['iid'], body['object_attributes']['source']['namespace'], body['object_attributes']['source_branch'], body['object_attributes']['target']['namespace'], body['object_attributes']['target_branch'], body['object_attributes']['url']))
					elif body['object_attributes']['action'] == 'merge':
						if body['user']['name'] == body['user']['username']:
							message.append("[{}7{}/{}\3] \2{}\2 merged a merge request: \2{}\2 (MR \2#{}\2) | Merging \2{}:{}\2 into \2{}:{}\2 | \2Link\2: {}".format("\3", body['project']['namespace'], body['project']['name'], body['user']['username'], body['object_attributes']['title'], body['object_attributes']['iid'], body['object_attributes']['source']['namespace'], body['object_attributes']['source_branch'], body['object_attributes']['target']['namespace'], body['object_attributes']['target_branch'], body['object_attributes']['url']))
						else:
							message.append("[{}7{}/{}\3] \2{}\2 \35({})\35 merged a merge request: \2{}\2 (MR \2#{}\2) | Merging \2{}:{}\2 into \2{}:{}\2 | \2Link\2: {}".format("\3", body['project']['namespace'], body['project']['name'], body['user']['name'], body['user']['username'], body['object_attributes']['title'], body['object_attributes']['iid'], body['object_attributes']['source']['namespace'], body['object_attributes']['source_branch'], body['object_attributes']['target']['namespace'], body['object_attributes']['target_branch'], body['object_attributes']['url']))
			elif 'event_name' in body:
				if body['object_kind'] == 'tag_push' and body['event_name'] == 'tag_push':
					# TODO: Implement this in a graceful manner
					return
				elif body['object_kind'] == 'push' and body['event_name'] == 'push':
					commits = body['commits']
					commits.reverse()
					for commit in commits:
						changes = ("\3" + "3 {}" + "\3" + "8 {}" + "\3" + "4 {}\3").format(len(commit['added']), len(commit['modified']), len(commit['removed']))
						commit_message = commit['message'].split('\n')[0].strip('\r')
						commit_url = "{}/commit/{}".format(body['project']['web_url'], commit['id'][:8])

						if body['user_name'] == body['user_username']:
							message.append("[{}7{}/{}\3] \2{}\2 pushed a commit: \2{}\2 | File Changes:{}\17 | ID: \2{}\2 | Link: \2{}\2".format("\3", body['project']['namespace'], body['project']['name'], body['user_username'], commit_message, changes, commit['id'][:8], commit_url))
						else:
							message.append("[{}7{}/{}\3] \2{}\2 \35({})\35 pushed a commit: \2{}\2 | File Changes:{}\17 | ID: \2{}\2 | Link: \2{}\2".format("\3", body['project']['namespace'], body['project']['name'], body['user_name'], body['user_username'], commit_message, changes, commit['id'][:8], commit_url))

			if len(message) != 0:
				for m in message:
					serv.message(irc, GIT_CHANNEL, m)

	return Handler

class GitListenerServer:
	def __init__(self, irc, port=8000):
		self.irc = irc
		self.port = port
		HandlerClass = make_handler_class(self.irc)
		self.httpd = socketserver.TCPServer(("", self.port), HandlerClass)
		self.started = False

	@Threaded
	def start(self):
		self.started = True
		try:
			self.httpd.serve_forever()
		except KeyboardInterrupt:
			self.stop()

	def stop(self):
		if self.started:
			self.httpd.socket.close()
			self.httpd.shutdown()

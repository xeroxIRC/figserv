class FOperFactory:
	# TODO: Add permission helpers
	# TODO: Add recursive_get_permissions and recursive_get_inherits to get indirect inherits and permissions
	def __init__(self, filename, owners=[]):
		self.filename = filename
		self.fopers = []
		self.fclasses = []
		self.owners = owners
		self._init_file()
		self._perms = []
		self._checked_classes = []

	def _init_file(self):
		self.fopers.clear()
		self.fclasses.clear()
		with open(self.filename, 'r') as fp:
			for line in fp:
				line = line.strip('\n')
				if line.startswith("OPER"):
					self.fopers.append(FOper(line))
				elif line.startswith("CLASS"):
					self.fclasses.append(FOperClass(line))

		for foper in self.fopers:
			foper.fclass = self.get_fclass(foper.fclass)

	def get_foper(self, account):
		for foper in self.fopers:
			if foper.account == account:
				return foper
		return None

	def get_fclass(self, name):
		for fclass in self.fclasses:
			if fclass.name == name:
				return fclass
		return None

	def has_permission(self, account, permission):
		if account in self.owners:
			return True

		foper = self.get_foper(account)
		if foper == None:
			return False
		else:
			fclass = foper.fclass
			self._perms.clear()
			self._checked_classes.clear()

			self.recursive_get_permissions(fclass)
			return permission in self._perms

	def recursive_get_permissions(self, fclass):
		if fclass.name in self._checked_classes:
			return
		self._checked_classes.append(fclass.name)

		for perm in fclass.permissions:
			if not perm in self._perms:
				self._perms.append(perm)

		if fclass.inherits != None:
			for inherits in fclass.inherits:
				self.recursive_get_permissions(self.get_fclass(inherits))

	def flush(self):
		with open(self.filename, 'w') as fp:
			for foper in self.fopers:
				fp.write("OPER {} {}\n".format(foper.account, foper.fclass.name))

			for fclass in self.fclasses:
				if fclass.inherits == None:
					fp.write("CLASS {} {}".format(fclass.name, ','.join(fclass.permissions)))
				else:
					fp.write("CLASS {} {} INHERIT {}".format(fclass.name, ','.join(fclass.permissions), ','.join(fclass.inherits)))

	# FOper Helpers
	def add_foper(self, account, clazz):
		for foper in self.fopers:
			if foper.account == account:
				return 1
		if self.get_fclass(clazz) == None:
			return 2
		self.fopers.append(FOper("OPER {} {}".format(account, clazz)))
		self.fopers[len(self.fopers)-1].fclass = self.get_fclass(clazz)
		return 0

	def set_foperclass(self, account, clazz): # Now we *could* go all C `stdlib' and return some value no one will ever use... or we could not
		foper = self.get_foper(account)
		if foper == None:
			return 1
		if foper.fclass.name == clazz:
			return 2

		fclass = self.get_fclass(clazz)
		if fclass == None:
			return 3
		foper.fclass = fclass
		return 0

	def add_permission(self, clazz, permission):
		fclass = self.get_fclass(clazz)
		if fclass == None:
			return 1

		if permission in fclass.permissions:
			return 2

		fclass.permissions.append(permission)
		return 0

	def remove_permission(self, clazz, perm_or_idx):
		fclass = self.get_fclass(clazz)
		if fclass == None:
			return 2

		if type(perm_or_idx).__name__ == "int":
			if perm_or_idx >= len(fclass.permissions):
				return 1
			fclass.permissions.pop(perm_or_idx)
			return 0
		elif type(perm_or_idx).__name__ == "str":
			if perm_or_idx in fclass.permissions:
				i = 0
				for perm in fclass.permissions:
					if perm == perm_or_idx:
						fclass.permissions.pop(i)
					i += 1
			else:
				return 1
		return 0

	def remove_foper(self, account):
		foper = self.get_foper(account)
		if foper == None:
			return 1

		i = 0
		for fo in self.fopers:
			if fo.account == account:
				self.fopers.pop(i)
			i += 1

	def add_fclass(self, classname, permissions, inherits=None):
		if self.get_fclass(classname) != None:
			return 1

		if inherits == None:
			self.fclasses.append(FOperClass("CLASS {} {}".format(classname, ','.join(permissions))))
		else:
			self.fclasses.append(FOperClass("CLASS {} {} INHERIT {}".format(classname, ','.join(permissions), ','.join(inherits))))

	def add_inherit():
		return

	def remove_inherit():
		return

class FOper:
	def __init__(self, line):
		splt = line.split(' ')
		# Line example: OPER bigfoot Owner
		self.account = splt[1]
		self.fclass = splt[2]

class FOperClass:
	def __init__(self, line):
		splt = line.split(' ')
		# Line example: CLASS Owner perm1,perm2 INHERIT class1,class2

		self.name = ""
		self.permissions = []
		self.inherits = None
		if splt[0] == "CLASS":
			self.name = splt[1]
			self.permissions = splt[2].split(',')
			if len(splt) == 5 and splt[3] == "INHERIT":
				self.inherits = splt[4].split(',')

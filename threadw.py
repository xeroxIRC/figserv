# Simple Python3 multithreading wrapper
#
# Add to the beginning of scripts or use with import.
#
#
# The MIT License
#
# Copyright © 2018 by luk3yx
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.
#

import threading

# A seamless threading function wrapper.
class Threaded:
    thread = None

    def __call__(self, *args, **kwargs):
        t = threading.Thread(target = self.func,
            args = args, kwargs = kwargs)
        t.start()
        self.thread = t
        return t

    def foreground(self, *args, **kwargs):
        if not self.thread or not self.thread.isAlive:
            self(*args, **kwargs)
        return self.thread.join()

    def __init__(self, func):
        self.func = func

# Only allow one thread per function, and if that thread is taken, return the
#   threading object from the current thread.
class SingleThread(Threaded):
    def __call__(self, *args, **kwargs):
        if self.thread and self.thread.isAlive:
            return self.thread
        return Threaded.__call__(self, *args, **kwargs)

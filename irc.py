import socket
from datetime import datetime
from time import sleep
from time import time
from time import gmtime
from time import strftime
import foper
from threadw import Threaded
from ast import literal_eval
import requests
import re
from hashlib import sha512
import secret
import git
from importlib import reload

owneracct = ['bigfoot', 'bigfoot-bots']
botchan = "#figserv-log"

ntsh_channels = []
ntsh_file = 'ntsh.db'

sed_channels = []
sed_file = 'sedserv.db'

ctcp_data = []
ctcp_file = 'ctcp.db'

sed_chathistory = {} # I plan on having a dictionary of arrays of tuples... examples: {'#opers': [('9217439', 'This is a message'), ('999999999', "abc")]}
HISTORY_LINES = 20

queued_accountnames = {}

TOR_SERVER = '804'

GIT_CHANNEL = "#figserv"

def get_user_hash(usr):
	if usr.uid.startswith(TOR_SERVER) and 'accountname' in usr.metadata:
		return sha512("{}".format(usr.metadata['accountname']).encode("ascii", "replace")).hexdigest()
	else:
		return sha512("{}@{}".format(usr.ident.strip('~'), usr.rhost).encode("ascii", "replace")).hexdigest()

def in_ctcp_db(usr):
	hash = get_user_hash(usr) # Cache the hash so we don't calculate it every iteration as it's an expensive function
	for record in ctcp_data:
		if record['user'] == hash:
			return True
	return False

def in_channel_ci(channels, c):
	for chan in channels:
		if chan.lower() == c.lower():
			return True
	return False

def get_or_default(table, key, default):
	table[key] = table[key] or default

def set_chr(inpt, pos, to):
	l = list(inpt)
	l[pos] = to
	return ''.join(l)

def pop(yayzw, idx):
	return yayzw[:idx] + yayzw[idx+1:]

def is_int(obj):
	try:
		int(obj)
	except:
		return False
	return True

def read_ctcp():
	starttime = time()
	ctcp_data.clear()
	with open(ctcp_file, 'r') as fp:
		for line in fp:
			splt = line.split(' ', maxsplit=2)
			if len(splt) == 1:
				ctcp_data.append({'user': splt[0], 'ts': splt[1], 'res': None})
			else:
				ctcp_data.append({'user': splt[0], 'ts': splt[1], 'res': literal_eval(splt[2])})
	return time() - starttime

def flush_ctcp():
	starttime = time()
	with open(ctcp_file, 'w') as fp:
		for record in ctcp_data:
			if record['res'] == None:
				fp.write("{} {}\n".format(record['user'], record['ts']))
			else:
				fp.write("{} {} {}\n".format(record['user'], record['ts'], repr(record['res'])))
	return time() - starttime

# TODO: No need to pass filename here
def read_sed(filename):
	sed_channels.clear()
	with open(filename, 'r') as fp:
		for line in fp:
			if line == "":
				continue
			sed_channels.append(line.strip('\n'))

def flush_sed(filename):
	with open(filename, 'w') as fp:
		for chan in sed_channels:
			if chan == "":
				continue
			fp.write(chan + '\n')

def read_ntsh(filename):
	ntsh_channels.clear()
	with open(filename, 'r') as fp:
		for line in fp:
			if line == "":
				continue
			ntsh_channels.append(line.strip('\n'))

def flush_ntsh(filename):
	with open(filename, 'w') as fp:
		for chan in ntsh_channels:
			if chan == "":
				continue
			fp.write(chan + '\n')

@Threaded
def figserv_on_cmd(serv, irc, sender, args):
	if not "accountname" in sender.metadata:
		sender.notice(irc, "You are not authorized to used this service. (Are you logged in?)", by=serv.uid)

	if irc.operfactory.has_permission(sender.metadata["accountname"], "figserv:use"):
		argv = args.split(' ')
		cmd = argv[0].upper()
		argc = len(argv) - 1
		res_invalid_usage = "Invalid usage. Try \2/msg FigServ HELP {}\2 for help."

		if cmd == "HELP":
			if argc == 0:
				sender.notice(irc, "List of commands for service \2FigServ\2:", by=serv.uid)
				sender.notice(irc, "\2DIE         \2Stops services", by=serv.uid)
				sender.notice(irc, "\2HELP        \2Gives canonical help information", by=serv.uid)
				sender.notice(irc, "\2FOPER       \2Manages FigServ operators", by=serv.uid)
				sender.notice(irc, "End of command list", by=serv.uid)
			elif argc == 1:
				helpcmd = argv[1].upper()
				if helpcmd == "HELP":
					sender.notice(irc, "Syntax for \2HELP\2:", by=serv.uid)
					sender.notice(irc, "/msg FigServ HELP [command]", by=serv.uid)
					sender.notice(irc, "-", by=serv.uid)
					sender.notice(irc, "Arguments:", by=serv.uid)
					sender.notice(irc, "[command] (optional):", by=serv.uid)
					sender.notice(irc, "    Command to provide extra information on", by=serv.uid)
					sender.notice(irc, "    If this is left empty, it gives a list of commands", by=serv.uid)
					sender.notice(irc, "End of help for \2HELP\2.", by=serv.uid)
				elif helpcmd == "FOPER":
					# TODO: Finish FOPER help
					sender.notice(irc, "Syntax for \2FOPER\2:", by=serv.uid)
					sender.notice(irc, "/msg FigServ FOPER ADDOPER <account> <class>", by=serv.uid)
					sender.notice(irc, "/msg FigServ FOPER DELOPER <account>", by=serv.uid)
					sender.notice(irc, "/msg FigServ FOPER SETCLASS <account> <class>", by=serv.uid)
					sender.notice(irc, "then continue listing subcommands...", by=serv.uid)
					sender.notice(irc, "-", by=serv.uid)
					sender.notice(irc, "Subcommands:", by=serv.uid)
					sender.notice(irc, "ADDOPER <account> <class>", by=serv.uid)
					sender.notice(irc, "    <account>: The account name of the user to add", by=serv.uid)
					sender.notice(irc, "    <class>: The class to add <account> to", by=serv.uid)
					sender.notice(irc, "-", by=serv.uid)
					sender.notice(irc, "then continue listing subcommands...", by=serv.uid)
					sender.notice(irc, "End of help for \2FOPER\2.", by=serv.uid)
				elif helpcmd == "DIE":
					sender.notice(irc, "Syntax for \2DIE\2:", by=serv.uid)
					sender.notice(irc, "/msg FigServ DIE", by=serv.uid)
				else:
					sender.notice(irc, "This help topic doesn't exist.", by=serv.uid)
			else:
				sender.notice(irc, res_invalid_usage.format("HELP"), by=serv.uid)
		elif cmd == "RELOAD":
			if not irc.operfactory.has_permission(sender.metadata['accountname'], "figserv:reload"):
				sender.notice(irc, "You are not authorized to run this command. Thanks for playing!", by=serv.uid)
				return

			if argc == 0:
				sender.notice(irc, "Reloading services...", by=serv.uid)
				sender.notice(irc, "Services are being reloaded NOW!")
				starttime = time()
				reload(git)
				sender.notice(irc, "Reload complete.")
				sender.notice(irc, "Reload finished, took {}ms.".format(int((time() - starttime) * 1000)), by=serv.uid)
		elif cmd == "DIE":
			if not irc.operfactory.has_permission(sender.metadata['accountname'], "figserv:die"):
				sender.notice(irc, "You are not authorized to run this command. Thanks for playing!", by=serv.uid)
				return

			if argc == 0:
				sender.notice(irc, "Services shutting down. ;-;", by=serv.uid)
				sender.notice(irc, "Quitting services then exiting...", by=irc.config['servid'])
				irc.stop()
				exit(0)
			else:
				sender.notice(irc, res_invalid_usage.format("DIE"), by=serv.uid)
		elif cmd == "FOPER":
			if not irc.operfactory.has_permission(sender.metadata['accountname'], "figserv:foper"):
				sender.notice(irc, "You do not have permission to use this command.", by=serv.uid)
				return
			if argc > 0:
				subcmd = argv[1].upper()
				if subcmd == "HELP" and argc == 1:
					sender.notice(irc, "To get help on this command, try \2/msg FigServ HELP FOPER\2.", by=serv.uid)
				elif subcmd == "ADDOPER" and argc == 3:
					ret = irc.operfactory.add_foper(argv[2], argv[3])
					if ret == 1:
						sender.notice(irc, "An operator by the name \2{}\2 already exists.".format(argv[2]), by=serv.uid)
					elif ret == 2:
						sender.notice(irc, "That class \2{}\2 doesn't exist.".format(argv[3]), by=serv.uid)
					elif ret == 0:
						sender.notice(irc, "An operator named \2{}\2 was added with class \2{}\2.".format(argv[2], argv[3]), by=serv.uid)
					irc.operfactory.flush()
				elif subcmd == "DELOPER" and argc == 2:
					ret = irc.operfactory.remove_foper(argv[2])
					if ret == 1:
						sender.notice(irc, "An operator by the name \2{}\2 does not exist.".format(argv[2]), by=serv.uid)
					elif ret == 0:
						sender.notice(irc, "Removed operator \2{}\2.".format(argv[2]), by=serv.uid)
				elif subcmd == "SETCLASS" and argc == 3:
					ret = irc.operfactory.set_foperclass(argv[2], argv[3])
					if ret == 3:
						sender.notice(irc, "There is no such fclass with the name \2{}\2.".format(argv[3]), by=serv.uid)
					elif ret == 2:
						sender.notice(irc, "The operator \2{}\2 already has the class \2{}\2.".format(argv[2], argv[3]), by=serv.uid)
					elif ret == 1:
						sender.notice(irc, "The operator \2{}\2 doesn't exist.".format(argv[2]), by=serv.uid)
					elif ret == 0:
						sender.notice(irc, "The operator \2{}\2 now has the class \2{}\2.".format(argv[2], argv[3]), by=serv.uid)
					irc.operfactory.flush()
				elif subcmd == "ADDCLASS" and argc == 3 or argc == 4:
					# TODO: Be careful, you need to worry about circular inheritance
					if argc == 3:
						ret = irc.operfactory.add_fclass(argv[2], argv[3].split(','))
						if ret == 1:
							sender.notice(irc, "A class with the name \2{}\2 already exists.".format(argv[2]), by=serv.uid)
						elif ret == 0:
							sender.notice(irc, "A class with the name \2{}\2 was created.".format(argv[2]), by=serv.uid)
					else:
						ret = irc.operfactory.add_fclass(argv[2], argv[3].split(','), argv[4].split(','))
						if ret == 1:
							sender.notice(irc, "A class with the name \2{}\2 already exists.".format(argv[2]), by=serv.uid)
						elif ret == 0:
							sender.notice(irc, "A class with the name \2{}\2 was created.".format(argv[2]), by=serv.uid)
					irc.operfactory.flush()
				elif subcmd == "LISTOPER" and argc == 1:
					sender.notice(irc, "Listing all FOPERs:", by=serv.uid)
					for foper in irc.operfactory.fopers:
						sender.notice(irc, "Account: \2{}\2, Class: \2{}\2".format(foper.account, foper.fclass.name), by=serv.uid)
					sender.notice(irc, "End of FOPER list", by=serv.uid)
				elif subcmd == "GRANT" and argc == 3:
					ret = irc.operfactory.add_permission(argv[2], argv[3])
					if ret == 1:
						sender.notice(irc, "The class \2{}\2 does not exist.".format(argv[2]), by=serv.uid)
					elif ret == 2:
						sender.notice(irc, "The class \2{}\2 already has the permission \2{}\2.".format(argv[2], argv[3]), by=serv.uid)
					elif ret == 0:
						sender.notice(irc, "The permission \2{}\2 has been added to the class \2{}\2.".format(argv[3], argv[2]), by=serv.uid)
					irc.operfactory.flush()
				elif subcmd == "REVOKE" and argc == 3:
					if is_int(argv[2]):
						fclass = irc.operfactory.get_fclass(argv[2])
						if fclass == None:
							self.notice(irc, "The class\2{}\2 does not exist.".format(argv[2]), by=serv.uid)
						else:
							perms = fclass.permissions
							ret = irc.operfactory.remove_permission(argv[2], int(argv[3]))
							if ret == 1:
								sender.notice(irc, "The number \2{}\2 is out of range.".format(argv[3]), by=serv.uid)
							elif ret == 0:
								sender.notice(irc, "Revoked \2{}\2 from \2{}\2.".format(perms[int(argv[3])], argv[2]), by=serv.uid)
					else:
						ret = irc.operfactory.remove_permission(argv[2], argv[3])
						if ret == 2:
							sender.notice(irc, "The class \2{}\2 does not exist.".format(argv[2]), by=serv.uid)
						elif ret == 1:
							sender.notice(irc, "The class \2{}\2 does not have the permission \2{}\2.".format(argv[2], argv[3]), by=serv.uid)
						elif ret == 0:
							sender.notice(irc, "Revoked \2{}\2 from \2{}\2.".format(argv[3], argv[2]), by=serv.uid)
					irc.operfactory.flush()
				elif subcmd == "LISTCLASS" and argc == 1:
					# TODO: Add indirect inherits and permissions
					sender.notice(irc, "Listing all foper classes:", by=serv.uid)
					for fclass in irc.operfactory.fclasses:
						if fclass.inherits == None:
							sender.notice(irc, "Class name: \2{}\2, Permissions: \2{}\2".format(fclass.name, len(fclass.permissions)), by=serv.uid)
						else:
							sender.notice(irc, "Class name: \2{}\2, Permissions: \2{}\2, Inherits: \2{}\2".format(fclass.name, len(fclass.permissions), len(fclass.inherits)), by=serv.uid)
					sender.notice(irc, "End of oper class list.", by=serv.uid)
				elif subcmd == "CLASSINFO" and argc == 2:
					# TODO: Add indirect inherits and permissions
					fclass = irc.operfactory.get_fclass(argv[2])
					if fclass == None:
						sender.notice(irc, "The class \2{}\2 does not exist.".format(argv[2]), by=serv.uid)
					else:
						sender.notice(irc, "Information on the class \2{}\2:".format(argv[2]), by=serv.uid)
						sender.notice(irc, "Class name: \2{}\2".format(fclass.name), by=serv.uid)
						sender.notice(irc, "Permissions:", by=serv.uid)
						i = 1
						for permission in fclass.permissions:
							sender.notice(irc, "Direct [{}]: \2{}\2".format(i, permission), by=serv.uid)
							i += 1

						sender.notice(irc, "Inherits:", by=serv.uid)
						if fclass.inherits != None:
							i = 1
							for inherit in fclass.inherits:
								sender.notice(irc, "Direct [{}]: \2{}\2".format(i, inherit), by=serv.uid)
								i += 1
						sender.notice(irc, "End of class information.", by=serv.uid)
				elif subcmd == "REHASH" and argc == 1:
					irc.operfactory._init_file()
					sender.notice(irc, "Rehashed fopers.db", by=serv.uid)
				elif subcmd == "FLUSH" and argc == 1:
					irc.operfactory.flush()
					sender.notice(irc, "Flushed FOPER data to file.", by=serv.uid)
				else:
					sender.notice(irc, "Invalid usage or subcommand. Try \2/msg FigServ HELP FOPER\2 for help.", by=serv.uid)
			else:
				sender.notice(irc, res_invalid_usage.format("FOPER"), by=serv.uid)
		else:
			sender.notice(irc, "Unknown command", by=serv.uid)
	else:
		sender.notice(irc, "You are not authorized to use this service.", by=serv.uid)

def ntsh_on_cmd(serv, irc, sender, args):
	if not "accountname" in sender.metadata:
		sender.notice(irc, "You are not authorized to use this service. (Are you logged in?)", by=serv.uid)

	if irc.operfactory.has_permission(sender.metadata['accountname'], "ntsh:use"):
		argv = args.split(' ')
		argc = len(argv) - 1
		cmd = argv[0].upper()
		res_invalid_usage = "Invalid usage. Try \2/msg NothingToSeeHere HELP {}\2 for help."

		# TODO: Save channels and allow channel flushing
		if cmd == "ADDCHAN":
			if irc.operfactory.has_permission(sender.metadata['accountname'], "ntsh:chans"):
				if argc == 1:
					# TODO: Maybe check for CBANs?
					if argv[1] == botchan:
						sender.notice(irc, "Unable to join \2{}\2.".format(argv[1]), by=serv.uid)
					else:
						if in_channel_ci(ntsh_channels, argv[1]):
							sender.notice(irc, "I'm already in this channel.", by=serv.uid)
						else:
							if argv[1].startswith('#'):
								ntsh_channels.append(argv[1].lower())
								irc.service_users['NTSH'].join(irc, argv[1].lower())
								sender.notice(irc, "Joined \2{}\2 and am now lurking.".format(argv[1]), by=serv.uid)
								flush_ntsh(ntsh_file)
							else:
								sender.notice(irc, "Invalid channel name", by=serv.uid)
				else:
					sender.notice(irc, res_invalid_usage.format("ADDCHAN"), by=serv.uid)
			else:
				sender.notice(irc, "You are not authorized to perform this command.", by=serv.uid)
		elif cmd == "DELCHAN":
			if irc.operfactory.has_permission(sender.metadata['accountname'], "ntsh:chans"):
				if argc == 1:
					if argv[1].lower() == botchan.lower():
						sender.notice(irc, "Unable to leave \2{}\2, that is my logging channel.".format(argv[1]), by=serv.uid)
					else:
						if in_channel_ci(ntsh_channels, argv[1]):
							serv.part(irc, argv[1], message="Released from channel")
							sender.notice(irc, "Left channel \2{}\2.".format(argv[1]), by=serv.uid)
							i = 0
							for c in ntsh_channels:
								if c.lower() == argv[1].lower():
									ntsh_channels.pop(i)
									break
								i += 1
							flush_ntsh(ntsh_file)
						else:
							sender.notice(irc, "I'm not in that channel.", by=serv.uid)
				else:
					sender.notice(irc, res_invalid_usage.format("DELCHAN"), by=serv.uid)
			else:
				sender.notice(irc, "You are not authorized to perform this command.", by=serv.uid)
		elif cmd == "FLUSH":
			if irc.operfactory.has_permission(sender.metadata['accountname'], "ntsh:flush"):
				if argc == 0:
					flush_ntsh(ntsh_file)
					sender.notice(irc, "Flushed NothingToSeeHere data to disk.", by=serv.uid)
				else:
					sender.notice(irc, res_invalid_usage.format("FLUSH"), by=serv.uid)
			else:
				sender.notice(irc, "You are not authorized to use this service.", by=serv.uid)
		elif cmd == "LISTCHAN":
			if irc.operfactory.has_permission(sender.metadata['accountname'], "ntsh:listchan"):
				if argc == 0:
					sender.notice(irc, "Listing watched channels:", by=serv.uid)
					for chan in ntsh_channels:
						sender.notice(irc, "Name: \2{}\2".format(chan), by=serv.uid)
					sender.notice(irc, "End of list of watched channels.", by=serv.uid)
				else:
					sender.notice(irc, res_invalid_usage.format("LISTCHAN"), by=serv.uid)
			else:
				sender.notice(irc, "You are not authorized to use this service.", by=serv.uid)
		elif cmd == "HELP":
			if argc == 0:
				sender.notice(irc, "Listing commands for service \2NothingToSeeHere\2:", by=serv.uid)
				sender.notice(irc, "\2HELP        \2Gives canonical help information", by=serv.uid)
				sender.notice(irc, "\2ADDCHAN     \2Adds a channel to lurk in", by=serv.uid)
				sender.notice(irc, "\2DELCHAN     \2Remove a channel from the lurk list", by=serv.uid)
				sender.notice(irc, "\2LISTCHAN    \2List channels that are being watched", by=serv.uid)
				sender.notice(irc, "\2FLUSH       \2Flush list of channels to disk", by=serv.uid)
				sender.notice(irc, "End of command list", by=serv.uid)
			elif argc == 1:
				sender.notice(irc, "Not yet fully implemented.", by=serv.uid)
				helpcmd = argv[1].upper()
				# TODO: Add all the help topics
				if helpcmd == "HELP":
					sender.notice(irc, "Syntax for \2HELP\2:", by=serv.uid)
					sender.notice(irc, "/msg NothingToSeeHere HELP [command]", by=serv.uid)
					sender.notice(irc, "-", by=serv.uid)
					sender.notice(irc, "Arguments:", by=serv.uid)
					sender.notice(irc, "[command] (optional):", by=serv.uid)
					sender.notice(irc, "    Command to provide extra information on", by=serv.uid)
					sender.notice(irc, "    If this is left empty, it gives a list of commands", by=serv.uid)
					sender.notice(irc, "End of help for \2HELP\2.", by=serv.uid)
				else:
					sender.notice(irc, "This help topic doesn't exist.", by=serv.uid)
			else:
				sender.notice(irc, res_invalid_usage.format("HELP"), by=serv.uid)
		else:
			sender.notice(irc, "Unknown command", by=serv.uid)
	else:
		sender.notice(irc, "You are not authorized to use this service.", by=serv.uid)
	return

# TODO: Make sure to add the ability for FOPERs to remove and block channels
@Threaded
def sedserv_on_cmd(serv, irc, sender, args): # Use re.sub("pattern", "replace_with", "input", count=1) (count could be zero if /g supplied)
	argv = args.split(' ')
	argc = len(argv) - 1
	cmd = argv[0].upper()
	res_invalid_usage = "Invalid usage. Try \2/msg SedServ HELP {}\2 for help."

	if cmd == "JOIN" or cmd == "REQUEST":
		if argc == 1:
			if not argv[1].startswith('#'):
				sender.notice(irc, "Invalid channel name.", by=serv.uid)
				return
			if in_channel_ci(sed_channels, argv[1]):
				sender.notice(irc, "I'm already in that channel.", by=serv.uid)
			else:
				if "accountname" in sender.metadata:
					has_permission = False
					sender.notice(irc, "Checking if you are the channel founder...", by=serv.uid)
					try:
						response = literal_eval(requests.post(secret.csurl, data={'key': secret.cskey, 'channel': argv[1], 'user': sender.metadata['accountname']}).text)
						if ('error' in response and response['error'] != None) and response['error'].startswith('\2'):
							sender.notice(irc, response['error'], by=serv.uid)
							return
						elif 'error' in response and response['error'] != None:
							sender.notice(irc, "Unable to fetch your channel access.", by=serv.uid)
							return

						if not 'F' in response['flags']:
							sender.notice(irc, "You are not authorized to add SedServ to \2{}\2.".format(argv[1]), by=serv.uid)
						else:
							sed_channels.append(argv[1].lower())
							serv.join(irc, argv[1], opme=True)
							flush_sed(sed_file)
							sender.notice(irc, "Joined channel \2{}\2.".format(argv[1]), by=serv.uid)
							irc.slog("{} JOIN: \2{}\2".format(sender.nick, argv[1]), 'SedServ')
					except Exception:
						sender.notice(irc, "An internal error occurred while attempting to fetch the access lists.", by=serv.uid)
				else:
					sender.notice(irc, "You must be registered and logged into services to run this command.", by=serv.uid)
		else:
			sender.notice(irc, res_invalid_usage.format("JOIN"), by=serv.uid)
	elif cmd == "LEAVE":
		if argc == 1:
			if in_channel_ci(sed_channels, argv[1]):
				if "accountname" in sender.metadata:
					sender.notice(irc, "Checking if you are the channel founder...", by=serv.uid)
					try:
						response = literal_eval(requests.post(secret.csurl, data={'key': secret.cskey, 'channel': argv[1], 'user': sender.metadata['accountname']}).text)
						if ('error' in response and response['error'] != None) and response['error'].startswith('\2'):
							sender.notice(irc, response['error'], by=serv.uid)
							return
						elif 'error' in response and response['error'] != None:
							sender.notice(irc, "Unable to fetch your channel access.", by=serv.uid)
							return

						if not 'F' in response['flags']:
							sender.notice(irc, "You are not authorized to remove SedServ from \2{}\2.".format(argv[1]), by=serv.uid)
						else:
							i = 0
							for c in sed_channels:
								if c.lower() == argv[1].lower():
									sed_channels.pop(i)
									break
								i += 1
							serv.part(irc, argv[1], message="Released from channel")
							flush_sed(sed_file)
							irc.slog("{} LEAVE: \2{}\2".format(sender.nick, argv[1]), 'SedServ')
					except Exception:
						sender.notice(irc, "An internal error occurred while attempting to fetch the access lists.", by=serv.uid)
				else:
					sender.notice(irc, "You must be registered and logged into services to run this command.", by=serv.uid)
			else:
				sender.notice(irc, "I'm not in that channel.", by=serv.uid)
		else:
			sender.notice(irc, res_invalid_usage.format("LEAVE"), by=serv.uid)
	elif cmd == "LISTCHAN":
		if "accountname" in sender.metadata:
			if irc.operfactory.has_permission(sender.metadata['accountname'], "sedserv:admin"):
				if argc == 0:
					sender.notice(irc, "I am in \2{}\2 channels. Here's a list: ".format(len(sed_channels)), by=serv.uid)
					i = 1
					for channel in sed_channels:
						sender.notice(irc, "{}. Channel name: \2{}\2".format(i, channel), by=serv.uid)
						i += 1
					sender.notice(irc, "End of channel list.", by=serv.uid)
					irc.slog("{} LISTCHAN".format(sender.nick), 'SedServ')
				else:
					sender.notice(irc, res_invalid_usage.format("LISTCHAN"), by=serv.uid)
			else:
				sender.notice(irc, "You do not have permission to perform this command.", by=serv.uid)
		else:
			sender.notice(irc, "You must be registered and identified with services to run this command.", by=serv.uid)
	elif cmd == "DELCHAN":
		if "accountname" in sender.metadata:
			if irc.operfactory.has_permission(sender.metadata['accountname'], "sedserv:admin"):
				if argc == 1:
					if in_channel_ci(sed_channels, argv[1]):
						i = 0
						for c in sed_channels:
							if c.lower() == argv[1].lower():
								sed_channels.pop(i)
								break
							i += 1
						serv.part(irc, argv[1], message="Removed from channel")
						flush_sed(sed_file)
						sender.notice(irc, "Left channel \2{}\2.".format(argv[1]), by=serv.uid)
						irc.slog("{} DELCHAN: \2{}\2".format(sender.nick, argv[1]), 'SedServ')
					else:
						sender.notice(irc, "I'm not in that channel.", by=serv.uid)
				else:
					sender.notice(irc, res_invalid_usage.format("REMOVE"), by=serv.uid)
			else:
				sender.notice(irc, "You do not have permission to perform this command.", by=serv.uid)
		else:
			sender.notice(irc, "You must be registered and identified with services to run this command.", by=serv.uid)
	elif cmd == "ADDCHAN":
		if "accountname" in sender.metadata:
			if irc.operfactory.has_permission(sender.metadata['accountname'], "sedserv:admin"):
				if argc == 1:
					if in_channel_ci(sed_channels, argv[1]):
						sender.notice(irc, "I'm already in that channel.", by=serv.uid)
					else:
						if not argv[1].startswith('#'):
							sender.notice(irc, "Invalid channel name.", by=serv.uid)
						else:
							sed_channels.append(argv[1])
							serv.join(irc, argv[1], opme=True)
							flush_sed(sed_file)
							sender.notice(irc, "Joined channel \2{}\2.".format(argv[1]), by=serv.uid)
							serv.notice(irc, argv[1], "Hello! I was added to this channel. One thing you should know, if you want me to leave, you may type \2/msg SedServ LEAVE {}\2, but you wouldn't want to do that anyway... would you?".format(argv[1]))
							irc.slog("{} ADDCHAN: \2{}\2".format(sender.nick, argv[1]), 'SedServ')
				else:
					sender.notice(irc, res_invalid_usage.format("ADD"), by=serv.uid)
			else:
				sender.notice(irc, "You are not authorized to perform this command.", by=serv.uid)
		else:
			sender.notice(irc, "You must be registered and identified with services to run this command.", by=serv.uid)
	else:
		sender.notice(irc, "Unknown command", by=serv.uid)

@Threaded
def sedserv_on_invite(serv, irc, sender, channel):
	try:
		response = literal_eval(requests.post(secret.csurl, data={'key': secret.cskey, 'channel': channel, 'user': sender.metadata['accountname']}).text)
		if ('error' in response and response['error'] != None) and response['error'].startswith('\2'):
			sender.notice(irc, response['error'], by=serv.uid)
			return
		elif 'error' in response and response['error'] != None:
			sender.notice(irc, "Unable to fetch your channel access.", by=serv.uid)
			return

		if not 'F' in response['flags']:
			sender.notice(irc, "You are not authorized to add SedServ to \2{}\2.".format(channel), by=serv.uid)
		else:
			sed_channels.append(channel)
			serv.join(irc, channel, opme=True)
			flush_sed(sed_file)
			sender.notice(irc, "Joined channel \2{}\2.".format(channel), by=serv.uid)
			irc.slog("{} JOIN: \2{}\2 (via INVITE)".format(sender.nick, channel), 'SedServ')
	except Exception as e:
		sender.notice(irc, "An internal error occurred while attempting to fetch the access lists.", by=serv.uid)
		print(e)

@Threaded
def sedserv_on_cmessage(serv, irc, target, sender, message):
	if in_channel_ci(sed_channels, target):
		ctcp = message.startswith("\1") and not message.startswith("\1ACTION")
		if not ctcp:
			if not target in sed_chathistory:
				sed_chathistory[target] = []
			sed_chathistory[target] = [(sender.uid, message)] + sed_chathistory[target]
			if len(sed_chathistory[target]) > HISTORY_LINES:
				while len(sed_chathistory[target]) > HISTORY_LINES:
					sed_chathistory[target].pop()

	if message.startswith("s/"):
		for char in message:
			slashes = []
			idx = 0
			escaped = False
			for char in message:
				if not escaped and char == '/':
					slashes.append(idx)

				if char == '\\' and not escaped:
					escaped = True
				else:
					escaped = False
				idx += 1

			if len(slashes) == 2 or len(slashes) == 3:
#				serv.message(irc, target, "Seems like valid SED, you might or might not have closed your sed expression...")
#				serv.message(irc, target, str(slashes))
				idx = 0
				splt = []
				prev = 0
				for char in message:
					if idx in slashes:
						if message[prev] == '/':
							splt.append(message[prev+1:idx])
						else:
							splt.append(message[prev:idx])
#						serv.message(irc, target, str(prev) + ", " + message[prev])
						prev = idx
					if idx == len(message) - 1 and not idx in slashes:
						splt.append(message[prev+1:])
#					serv.message(irc, target, message[idx])
					idx += 1
#				serv.message(irc, target, str(splt))

				idx = 0
				itemidx = 0
				escaped = False
				to_remove = []
				for item in splt:
					for char in item:
						if char == '\\' and not escaped:
							to_remove.append((itemidx, idx))
							escaped = True
						else:
							escaped = False
						idx += 1
					itemidx += 1
					idx = 0

				offset = 0
				for item in to_remove:
					splt[item[0]] = pop(splt[item[0]], item[1] - offset)
#					serv.message(irc, target, str(item))
					offset += 1
#				serv.message(irc, target, str(splt))

				if len(splt) == 3 or len(splt) == 4:
					found = False
					for linet in sed_chathistory[target]:
						case_insensitive = False
						if len(splt) == 4:
							if 'i' in splt[3]:
								case_insensitive = True
						if linet[0] == sender.uid:
							matches = None
							if case_insensitive:
								matches = re.findall(splt[1], linet[1], re.IGNORECASE)
							else:
								matches = re.findall(splt[1], linet[1])
							if len(matches) != 0 and linet[1] != message:
								found = True
								count = 1
								me = False
								if linet[1].startswith("\1ACTION"):
									me = True
								if len(splt) == 4:
									if 'g' in splt[3]:
										count = 0
								if me:
									if case_insensitive:
										serv.message(irc, target, "* \2{}\2 {}".format(sender.nick, re.sub(splt[1], splt[2], linet[1].strip('\1')[7:], count=count, flags=re.IGNORECASE)))
									else:
										serv.message(irc, target, "* \2{}\2 {}".format(sender.nick, re.sub(splt[1], splt[2], linet[1].strip('\1')[7:], count=count)))
								else:
									if case_insensitive:
										serv.message(irc, target, "<{}> {}".format(sender.nick, re.sub(splt[1], splt[2], linet[1], count=count, flags=re.IGNORECASE)))
									else:
										serv.message(irc, target, "<{}> {}".format(sender.nick, re.sub(splt[1], splt[2], linet[1], count=count)))
								irc.slog("SED by \2{}\2 (\2{}\2) on \2{}\2.".format(sender.uid, sender.get_rnuh(), target), "SedServ")
								#irc.log("sedserv_on_cmessage", "SED by \2{}\2 (\2{}\2) on \2{}\2.".format(sender.uid, sender.get_rnuh(), target))
								break

					if not found:
						serv.message(irc, target, "The pattern was not found in the last \2{}\2 lines.".format(HISTORY_LINES))
			return
	return

def floodserv_on_cmd(serv, irc, sender, args):
	return

def logserv_on_cmd(serv, irc, sender, args):
	return

def ctcp_on_cmd(serv, irc, sender, args):
	argv = args.split(' ')
	argc = len(argv) - 1
	cmd = argv[0].upper()
	res_invalid_usage = "Invalid usage. Try \2/msg CTCP HELP {}\2 for help."

	if cmd == "REFRESH":
		if not "accountname" in sender.metadata:
			sender.notice(irc, "You are not authorized to execute this command. (Are you logged in?)", by=serv.uid)
		else:
			if irc.operfactory.has_permission(sender.metadata['accountname'], "ctcp:refresh"):
				if argc == 0:
					for uid in irc.users:
						irc.fetch_version(uid, notify=False)
					sender.notice(irc, "Finished CTCP VERSIONing all known users.", by=serv.uid)
				else:
					sender.notice(irc, res_invalid_usage.format("REFRESH"), by=serv.uid)
			else:
				sender.notice(irc, "You are not authorized to perform this command.", by=serv.uid)
	elif cmd == "UPDATE":
		if argc == 0:
			irc.fetch_version(sender.uid, notify=False)
			sender.notice(irc, "Sent you a CTCP VERSION and updated your response.", by=serv.uid)
		else:
			sender.notice(irc, res_invalid_usage.format("UPDATE"), by=serv.uid)
		return
	elif cmd == "HASH":
		if not "accountname" in sender.metadata:
			sender.notice(irc, "You are not authorized to execute this command. (Are you logged in?)", by=serv.uid)
		else:
			if irc.operfactory.has_permission(sender.metadata['accountname'], "ctcp:hash"):
				if argc == 1:
					found = False
					for uid in irc.users:
						if uid == argv[1].upper() or irc.users[uid].nick.lower() == argv[1].lower():
							found = True
							sender.notice(irc, "Hash of user \2{}\2: \2{}\2".format(argv[1], get_user_hash(irc.users[uid])), by=serv.uid)
							break
					if not found:
						sender.notice(irc, "User not found. You may specify a UID or nick.", by=serv.uid)
				else:
					sender.notice(irc, res_invalid_usage.format("HASH"), by=serv.uid)
			else:
				sender.notice(irc, "You are not authorized to perform this command.", by=serv.uid)
	elif cmd == "VERSION":
		# TODO: Here and HASH, somehow make a wildcard for the argument (?ig?oot matches bigfoot and figboot)
		if not "accountname" in sender.metadata:
			sender.notice(irc, "You are not authorized to execute this command. (Are you logged in?)", by=serv.uid)
		else:
			if irc.operfactory.has_permission(sender.metadata['accountname'], "ctcp:getver"):
				if argc == 1:
					found = False
					hash = None
					for uid in irc.users:
						if uid == argv[1].upper() or irc.users[uid].nick.lower() == argv[1].lower():
							hash = get_user_hash(irc.users[uid])

					if hash == None:
						hash = argv[1] # If it's not an online user, maybe it's a hash?

					for record in ctcp_data:
						if record['user'] == hash:
							found = True
							sender.notice(irc, "Time: \2{}\2 \35({})\35 | Reply: \2{}\2".format(datetime.utcfromtimestamp(float(record['ts'])).strftime('%F %X'), record['ts'], repr(record['res'])[1:-1]), by=serv.uid)
							break

					if not found:
						sender.notice(irc, "No users found with that specification. You may specify a UID, nick, or hash.", by=serv.uid)
				else:
					sender.notice(irc, res_invalid_usage.format("VERSION"), by=serv.uid)
			else:
				sender.notice(irc, "You are not authorized to perform this command.", by=serv.uid)
	elif cmd == "REQUEST":
		if not "accountname" in sender.metadata:
			sender.notice(irc, "You are not authorized to execute this command. (Are you logged in?)", by=serv.uid)
		else:
			if irc.operfactory.has_permission(sender.metadata['accountname'], "ctcp:setver"):
				if argc == 1:
					found = False
					for uid in irc.users:
						if uid == argv[1].upper() or irc.users[uid].nick.lower() == argv[1].lower():
							found = True
							irc.fetch_version(uid, notify=False)
							sender.notice(irc, "Sent \2{}\2 a CTCP VERSION.".format(irc.users[uid].nick), by=serv.uid)
							break
					if not found:
						sender.notice(irc, "No user found with that specification.", by=serv.uid)
				else:
					sender.notice(irc, res_invalid_usage.format("REQUEST"), by=serv.uid)
			else:
				sender.notice(irc, "You are not authorized to perform this command.", by=serv.uid)
	else:
		sender.notice(irc, "Unknown command.", by=serv.uid)
	return

def git_on_cmd(serv, irc, sender, args):
	return

def held_on_cmd(serv, irc, sender, args):
	return

BUFFER_SIZE = 4096

class IRC:
	def __init__(self, config, sock=None):
		self.server_auth = False
		self.bursted = False
		self.socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
		self.config = config
		self.users = {}
		self.chan_ts = {}
		self.service_users = {}
		get_or_default(self.config, "host", "127.0.0.1")
		get_or_default(self.config, "port", 7001)
		get_or_default(self.config, "servname", "services.example.com")
		get_or_default(self.config, "servid", "420")
		get_or_default(self.config, "servdesc", "Bigfoot's 420 services")
		get_or_default(self.config, "uplinkname", "irc.example.com")
		get_or_default(self.config, "recvpass", "")
		get_or_default(self.config, "sendpass", "")
		self._curid = "999999"
		self.init_fopers('fopers.db')
		self.init_ntsh()
		self.init_sed()
		self.init_ctcp()
		self.gitl = git.GitListenerServer(self, port=secret.whport)
		self.gitl.start(self.gitl)

	def init_ntsh(self):
		read_ntsh(ntsh_file)

	def init_sed(self):
		read_sed(sed_file)

	def init_ctcp(self):
		rduration = read_ctcp()
		print("INFO: Took {}ms to read the CTCP database.".format(int(rduration * 1000)))

	def init_fopers(self, filename):
		self.operfactory = foper.FOperFactory(filename, owners=owneracct)

	def _sendraw(self, data, neg=False):
		data = data + "\n"
		self.socket.send(data.encode('UTF-8', 'replace'))
		if neg:
			print("NEG <- " + data.strip('\n'))
		else:
			print("<- " + data.strip('\n'))

	def connect(self):
		self.socket.connect((self.config['host'], self.config['port']))
		self.handle()

	def fetch_version(self, uid, notify=True):
		if uid.startswith("00A"):
			return
		self.service_users['CTCP'].ctcp(self, uid, 'VERSION')
		if notify:
			self.users[uid].notice(self, "Hello, \2{}\2! I'm your friendly neighborhood bot. I don't like to be called a bot though, so please call me a service. Thanks!".format(self.users[uid].nick), by=self.service_users['CTCP'].uid)
			self.users[uid].notice(self, "Right now, I am collecting anonymous statistics on some users that connect. Be sure to talk to bigfoot or any operators on #figserv if you want more information on the matter.", by=self.service_users['CTCP'].uid)
		self.users[uid].ctcp_requests['version'] = {'ts': int(time())}
		return

	def on_ctcp_reply(self, uid, rq, args):
		if rq.upper() == "VERSION":
			if 'version' in self.users[uid].ctcp_requests and self.users[uid].ctcp_requests['version']['ts'] + 5 >= int(time()):
				uh = get_user_hash(self.users[uid])
				idx = 0
				for record in ctcp_data:
					if record['user'] == uh:
						ctcp_data.pop(idx)
						break
					idx += 1
				ctcp_data.append({'user': uh, 'ts': int(time()), 'res': args})
				self.users[uid].ctcp_requests.pop('version')
				flush_ctcp()
				for u in self.users:
					if 'version' in self.users[u].ctcp_requests and self.users[u].ctcp_requests['version']['ts'] + 5 < int(time()):
						self.users[u].ctcp_requests.pop('version')

	def handle(self):
		mfile = self.socket.makefile()
		while True:
			try:
				line = mfile.readline().strip("\n")
			except UnicodeError:
				self.log("handle", "Error in handler, failed to read string from socket!")
				continue
			if line.startswith("CAPAB START") and not self.bursted:
				self.register()

			if self.bursted:
				#commands = data.split("\n")
				#for line in commands:
					# TODO: Handle MOTD
				if line == "":
					continue
				print("-> " + line)
				splt = line.split(" ")
				if len(splt) == 4 and splt[1] == "PING":
					self._sendraw(":{0} PONG {0} :{1}".format(self.config['servid'], splt[2]))
				elif len(splt) == 3 and splt[1] == "IDLE":
					self._sendraw(":{} IDLE {} 0 0".format(splt[2], splt[0][1:]))
				elif len(splt) == 3 and splt[1] == "OPERTYPE":
					self.users[splt[0][1:]].opertype = splt[2]
				elif len(splt) > 4 and splt[1] == "METADATA":
					# :00A METADATA 804AAAAG7 accountname :bigfoot-bots
					metacmd = line.split(' ', maxsplit=4)
					if metacmd[2] != "*":
						if metacmd[2].startswith('#'):
							# To be implemented
							none = None # Best NOP I could think up
						else:
							if metacmd[2] in self.users:
								if metacmd[4][1:] == "":
									self.users[metacmd[2]].metadata.pop(metacmd[3])
								else:
									self.users[metacmd[2]].metadata[metacmd[3]] = metacmd[4].strip(':')
							else:
								if metacmd[3] == "accountname":
									queued_accountnames[metacmd[2]] = metacmd[4].strip(':')
				elif len(splt) > 3 and splt[1] == "MODE":
					if 'o' in splt[3]:
						adding = True
						for char in splt[3]:
							if char == '-':
								adding = False
							elif char == '+':
								adding = True

							if not adding and char == 'o':
								self.users[metacmd[0][1:]].operclass = None
				elif len(splt) > 4 and splt[1] == "KICK":
					# :435AAAAAA KICK #services 435AAAAAA :a
					if splt[3].startswith(self.config['servid']):
						for serv in self.service_users:
							if self.service_users[serv].uid == splt[3]:
								self.service_users[serv].rejoin_chans(self)
				elif len(splt) > 3 and splt[1] == "KILL":
					if splt[2].startswith(self.config['servid']):
						for serv in self.service_users:
							if self.service_users[serv].uid == splt[2]:
								self.service_users[serv].connect(self)
								self.service_users[serv].rejoin_chans(self)
				elif len(splt) > 4 and splt[1] == "FJOIN": # TODO: Make sure that this TS is less than the original TS before setting
					#if splt[2].lower() in self.chan_ts:
					#	if int(splt[3]) < int(self.chan_ts[splt[2].lower()]):
					self.chan_ts[splt[2].lower()] = splt[3]
					#else:
					#	self.chan_ts[splt[2].lower()] = splt[3]
				elif len(splt) > 1 and splt[1] == "UID":
					# TODO: Here, check for expired records and see if we need to re-CTCP them
					# TODO: Also, somehow store if someone doesn't respond to a VERSION response
					self.users[splt[2]] = IRCUser(line)
					if self.users[splt[2]].uid in queued_accountnames:
						self.users[splt[2]].metadata['accountname'] = queued_accountnames[splt[2]]
						queued_accountnames.pop(splt[2])
					if not in_ctcp_db(self.users[splt[2]]):
						self.fetch_version(splt[2])
				elif len(splt) > 1 and splt[1] == "QUIT":
					self.users.pop(splt[0][1:])
				elif len(splt) > 3 and splt[1] == "NOTICE":
					if splt[2] == self.service_users['CTCP'].uid:
						realsplt = line.split(' ', maxsplit=3)
						message = realsplt[3][1:]
						if message.startswith("\1") and message.endswith("\1"):
							message = message.strip("\1")
							self.on_ctcp_reply(splt[0][1:], message.split(' ')[0], message.split(' ', maxsplit=1)[1])
				elif len(splt) > 3 and splt[1] == "PRIVMSG":
					if splt[2].startswith('#'):
						if in_channel_ci(sed_channels, splt[2]):
							sender = None
							for usr in self.users:
								if self.users[usr].uid == splt[0][1:]:
									sender = self.users[usr]
							message = line.split(' ', maxsplit=3)[3]
							if message[0] == ':':
								message = message[1:]
							sedserv_on_cmessage(self.service_users['SedServ'], self, splt[2], sender, message)
					else:
						for serv in self.service_users:
							if self.service_users[serv].uid == splt[2]:
								sender = None
								for usr in self.users:
									if self.users[usr].uid == splt[0][1:]:
										sender = self.users[usr]
								if sender == None:
									return

								if self.service_users[serv].on_command != None:
									if line.split(' ')[3].startswith(':'):
										self.service_users[serv].on_command(self.service_users[serv], self, sender, line.split(' ', maxsplit=3)[3][1:])
									else:
										self.service_users[serv].on_command(self.service_users[serv], self, sender, line.split(' ', maxsplit=3)[3])
				elif len(splt) > 2 and splt[1] == "FHOST":
					self.users[splt[0][1:]].vhost = splt[2].strip(':')
				elif len(splt) == 4 and splt[1] == "NICK":
					for usr in self.users:
						if self.users[usr].uid == splt[0][1:]:
							self.users[usr].nick = splt[2]
				elif len(splt) == 4 and splt[1] == "SAVE":
					# :435 SAVE 420AAAAAA 1537557963
					if splt[2] in self.users:
						self.users[splt[2]].nick = splt[2]
				elif len(splt) == 5 and splt[1] == "INVITE":
					# :435AAAB5N INVITE 420AAAAAD #chat 0
					if splt[2] == self.service_users['SedServ'].uid:
						if not splt[3] in sed_channels:
							serv = self.service_users['SedServ']
							sender = None
							if in_channel_ci(sed_channels, splt[3]):
								sender.notice(self, "I'm already in this channel. (I honestly don't know how you managed to do this...)", by=serv.uid)
								serv.join(self, splt[3], opme=True)
								continue

							for usr in self.users:
								if self.users[usr].uid == splt[0][1:]:
									sender = self.users[usr]

							if sender == None:
								continue

							if not "accountname" in sender.metadata:
								sender.notice(self, "You must be registered and logged into services to invite me to a channel.", by=serv.uid)
								continue
							sedserv_on_invite(serv, self, sender, splt[3].lower())
				# TODO: Handle nick changes from SAVE

			sleep(0.01)

	def register(self):
		# Capabilities Negotiation
		self._sendraw("CAPAB START 1202", True)
		self._sendraw("CAPAB CAPABILITIES :PROTOCOL=1202", True)
		self._sendraw("CAPAB END", True)

		# Server and bursting
		self._sendraw("SERVER {} {} 0 {} :{}".format(self.config['servname'], self.config['sendpass'], self.config['servid'], self.config['servdesc']), True)
		#data = self.socket.recv(BUFFER_SIZE)

		self.wait_for_burst()
		#sleep(0.1) # Wait half a second for the burst
		self.add_users()

	def wait_for_burst(self):
		bursting = True
		while bursting:
			data = self.socket.recv(BUFFER_SIZE).decode('UTF-8', 'replace').strip("\n")
			print("NEG -> " + data)

			if data.startswith("SERVER"):
				splt = data.split(" ")
				if splt[2] == self.config['sendpass'] and splt[1] == self.config['uplinkname']:
					self._sendraw("BURST", True)
					recvburst = True
					mfile = self.socket.makefile()
					while recvburst:
						try:
							line = mfile.readline().strip('\n')
						except UnicodeError:
							self.log("wait_for_burst", "Failed to read string during netburst!")
							continue
						splt = line.split(' ')
						print("NEG -> " + line)
						if len(splt) == 2 and splt[1] == "ENDBURST":
							recvburst = False
						elif len(splt) > 1 and splt[1] == "UID":
							self.users[splt[2]] = IRCUser(line)
						elif len(splt) > 4 and splt[1] == "FJOIN": # TODO: Make sure that this TS is less than the original TS before setting
							if splt[2].lower() in self.chan_ts:
								if int(splt[3]) < int(self.chan_ts[splt[2].lower()]):
									self.chan_ts[splt[2].lower()] = splt[3]
							else:
								self.chan_ts[splt[2].lower()] = splt[3]
						elif len(splt) > 4 and splt[1] == "METADATA":
							metacmd = line.split(' ', maxsplit=4)
							if metacmd[2] != "*":
								if metacmd[2].startswith('#'):
									none = None
								else:
									if metacmd[4][1:] == "":
										self.users[metacmd[2]].metadata.pop(metacmd[3])
									else:
										self.users[metacmd[2]].metadata[metacmd[3]] = metacmd[4][1:]
						elif len(splt) == 3 and splt[1] == "OPERTYPE":
							self.users[splt[0][1:]].opertype = splt[2]

					self._sendraw("ENDBURST", True)
					bursting = False
					self.bursted = True
				else:
					bursting = False
					print("Authentication failed! (Password or server name didn't match)")
					self.socket.close()
					exit()

			sleep(0.1)

	def log(self, method, message):
		if self.service_users['LogServ'].connected:
			self.service_users['LogServ'].message(self, botchan, "[\2{}\2] \2{}()\2: {}".format(strftime("%X", gmtime()), method, message))
		print("[{}] {}(): {}".format(strftime("%X", gmtime()), method, message.replace("\2", "")))

	def add_users(self):
		#self.service_users['DabServ'] = ServiceClient("DabServ", "dab.serv", "dab", "Bigfoot's Dab Services", on_cmd=figserv_on_cmd)
		#self.service_users['DabServ'].connect(self)
		#self.service_users['DabServ'].join(self, "#opers", True)

		#self._sendraw(":{} KILL {} :{}".format(self.config['servid'], "00AAAAABF", "Killed"))
		#self._sendraw(":{} KILL {} :{}".format(self.config['servid'], "00AAAAAAE", "Killed"))

		self.service_users['FigServ'] = ServiceClient("FigServ", "fig.xeroxirc.net", "figserv", "Bigfoot's IRC Services Manager", on_cmd=figserv_on_cmd)
		self.service_users['FigServ'].connect(self)
		self.service_users['FigServ'].join(self, botchan, opme=True)

#		self.service_users['NTSH'] = ServiceClient("NothingToSeeHere", "fig.xeroxirc.net", "ntsh", "Nothing to see here, move along...", on_cmd=ntsh_on_cmd)
#		self.service_users['NTSH'] = ServiceClient("`", "fig.xeroxirc.net", "ntsh", "Nothing to see here, move along...", on_cmd=ntsh_on_cmd)
#		self.service_users['NTSH'].connect(self)
#		self.service_users['NTSH'].join(self, botchan, opme=True)
#		for chan in ntsh_channels:
#			self.service_users['NTSH'].join(self, chan)
		#self.service_users['NTSH'].join(self, '#the-poolhouse', opme=False)

		self.service_users['FloodServ'] = ServiceClient("FloodServ", "fig.xeroxirc.net", "floodserv", "Bigfoot's IRC Flood Prevention Services", on_cmd=floodserv_on_cmd)
		self.service_users['FloodServ'].connect(self)
		self.service_users['FloodServ'].join(self, botchan, opme=True)

		self.service_users['SedServ'] = ServiceClient("SedServ", "fig.xeroxirc.net", "sedserv", "Bigfoot's SED Services", on_cmd=sedserv_on_cmd)
		self.service_users['SedServ'].connect(self)
		self.service_users['SedServ'].join(self, botchan, opme=True)
		for chan in sed_channels:
			self.service_users['SedServ'].join(self, chan, opme=True)

		self.service_users['LogServ'] = ServiceClient("LogServ", "fig.xeroxirc.net", "logserv", "Bigfoot's FigServ Logging", on_cmd=logserv_on_cmd)
		self.service_users['LogServ'].connect(self)
		self.service_users['LogServ'].join(self, botchan, opme=True)

		self.service_users['CTCP'] = ServiceClient("CTCP", "fig.xeroxirc.net", "ctcp", "Bigfoot's CTCP Services", on_cmd=ctcp_on_cmd)
		self.service_users['CTCP'].connect(self)
		self.service_users['CTCP'].join(self, botchan, opme=True)

		self.service_users['GitServ'] = ServiceClient("GitServ", "fig.xeroxirc.net", "git", "Bigfoot's FigServ Repo Tracker", on_cmd=git_on_cmd)
		self.service_users['GitServ'].connect(self)
		self.service_users['GitServ'].join(self, botchan, opme=True)
		self.service_users['GitServ'].join(self, GIT_CHANNEL, opme=True)

		nicks_to_hold = ['|', '^']

		for nick in nicks_to_hold:
			self.service_users[nick] = ServiceClient(nick, "fig.xeroxirc.net", "held", "Nick held until further notice", on_cmd=held_on_cmd)
			self.service_users[nick].connect(self)
			self.service_users[nick].join(self, botchan, opme=True)

	# TODO: Log levels?
	def slog(self, message, service='LogServ'):
		self._sendraw(":{} PRIVMSG {} :{}".format(self.service_users[service].uid, botchan, message))

	def nextchar(self, pos):
		if self._curid[pos] == '9':
			self._curid = set_chr(self._curid, pos, 'A')
			return True
		elif self._curid[pos] == 'Z':
			self._curid = set_chr(self._curid, pos, '0')
			return False
		else:
			self._curid = set_chr(self._curid, pos, chr(ord(self._curid[pos]) + 1))
			return False

	def nextid(self):
		if self._curid == "999999":
			self._curid = "AAAAAA"
			return
		for i in range(5, -1, -1):
			inc = self.nextchar(i)
			if not inc:
				return
			if inc and i > 0:
				j = 1
				while True:
					ninc = self.nextchar(i - j)
					if not ninc or self._curid == "999999":
						if self._curid == "999999":
							self._curid = "AAAAAA"
						return
			elif inc and i == 0:
				self._curid = "AAAAAA"
				return

	def stop(self, message="Services Stopping"):
		for usr in self.service_users:
			self.service_users[usr].quit(self, message)
		self.gitl.stop()
		self._sendraw(":{} ERROR :{}".format(self.config['servid'], "Services Stopping"))
		self.socket.close()

class ServiceClient:
	def __init__(self, nick, vhost, ident, realname, ip="0.0.0.0", umode="+Iiko", rhost=None, on_cmd=None):
		if rhost == None:
			rhost = vhost

		self.nick = nick
		self.vhost = vhost
		self.ident = ident
		self.ip = ip
		self.realname = realname
		self.umode = umode
		self.rhost = rhost
		self.connected = False
		self.channels = []
		if on_cmd != None:
			self.on_command = on_cmd

	def connect(self, irc):
		irc.nextid()
		ts = int(time())
		ts = 1
		self.ts = ts
		self.uid = irc.config['servid'] + irc._curid

		for usr in irc.users:
			if irc.users[usr].nick == self.nick:
				#irc._sendraw(":{} SVSNICK {} {} {}".format(irc.config['servid'], irc.users[usr].uid, irc.users[usr].uid, ts))
				#irc.users[usr].killraw(irc, "Nickname reclaimed by services")
				irc._sendraw(":{} KILL {} :{}".format(irc.config['servid'], irc.users[usr].uid, "Nickname reclaimed by services"))

		irc._sendraw(":{} UID {} {} {} {} {} {} {} {} {} :{}".format(irc.config['servid'], self.uid, ts, self.nick, self.rhost, self.vhost, self.ident, self.ip, ts, self.umode, self.realname))
		self.connected = True

	def join(self, irc, channel, opme=False, append=True):
		if self.connected:
			chants = 1
			newchan = False
			if not channel.lower() in irc.chan_ts:
				chants = int(time())
				irc.chan_ts[channel.lower()] = chants
				newchan = True
			else:
				chants = irc.chan_ts[channel.lower()]

			if newchan:
				irc._sendraw(":{} FJOIN {} {} {} :{}".format(irc.config['servid'], channel, chants, "+nt", "o," + self.uid))
			else:
				irc._sendraw(":{} FJOIN {} {} {} :{}".format(irc.config['servid'], channel, chants, "+", "," + self.uid))

			if opme:
				irc._sendraw(":{} FMODE {} {} {}".format(irc.config['servid'], channel, "1", "+o " + self.nick))
			if append:
				self.channels.append((channel.lower(), opme))

	def rejoin_chans(self, irc):
		for chan in self.channels:
			self.join(irc, chan[0], opme=chan[1], append=False)

	def quit(self, irc, message):
		irc._sendraw(":{} QUIT :{}".format(self.uid, message))
		self.connected = False

	def part(self, irc, channel, message="Leaving"):
		if not self.in_channel(channel):
			return 1

		if channel.lower() == botchan.lower():
			return 2

		i = 0
		popped = False
		for chantuple in self.channels:
			if chantuple[0].lower() == channel.lower():
				self.channels.pop(i)
				popped = True
				break
			i += 1

		if not popped:
			return 3
		irc._sendraw(":{} PART {} :{}".format(self.uid, channel, message))

	def in_channel(self, channel):
		for ctuple in self.channels:
			if channel.lower() == ctuple[0].lower():
				return True
		return False

	def message(self, irc, target, message):
		irc._sendraw(":{} PRIVMSG {} :{}".format(self.uid, target, message[:400]))

	def notice(self, irc, target, message):
		irc._sendraw(":{} NOTICE {} :{}".format(self.uid, target, message[:400]))

	def kill(self, irc, target, message):
		irc._sendraw(":{} KILL {} :Killed ({} ({}))".format(self.uid, target, self.nick, message))

	def killraw(self, irc, target, message):
		irc._sendraw(":{} KILL {} :{}".format(irc.config['servid'], target, message))

	def ctcp(self, irc, to, rq, args=None):
		if args == None:
			self.message(irc, to, "\1{}\1".format(rq))
		else:
			self.message(irc, to, "\1{} {}\1".format(rq, args))

class IRCUser:
	def __init__(self, cmd):
		args = cmd.split(' ')
		sno = None
		self.metadata = {}
		self.ctcp_requests = {}
		if 's' in args[10]:
			args = cmd.split(' ', maxsplit=12)
			sno = True
		else:
			args = cmd.split(' ', maxsplit=11)
			sno = False

			self.metadata = {}
			self.operclass = None

		if sno: # User has SNOmasks
			#:00A UID 00AAAAAAD 1535746710 Global services.int services.int Global 0.0.0.0 1535746710 +Iiko :Network Announcements
			self.sid = args[0][1:]
			self.uid = args[2]
			self.ts = args[3]
			self.nick = args[4]
			self.rhost = args[5]
			self.vhost = args[6]
			self.ident = args[7]
			self.ip = args[8]
			self.umode = args[10]
			self.snomask = args[11]
			self.realname = args[12][1:]
		else: # User has no SNOmasks
			self.sid = args[0][1:]
			self.uid = args[2]
			self.ts = args[3]
			self.nick = args[4]
			self.rhost = args[5]
			self.vhost = args[6]
			self.ident = args[7]
			self.ip = args[8]
			self.umode = args[10]
			self.realname = args[11][1:]

	def get_nuh(self):
		return "{}!{}@{}".format(self.nick, self.ident, self.vhost)

	def get_rnuh(self):
		return "{}!{}@{}".format(self.nick, self.ident, self.rhost)

	def killraw(self, irc, message):
		irc._sendraw(":{} KILL {} :{}".format(irc.config['servid'], self.uid, message))

	def kill(self, irc, message, by=None):
		if by == None:
			by = irc.config['servname']
		irc._sendraw(":{} KILL {} :{}".format(irc.config['servid'], self.uid, "Killed ({} ({}))".format(by, message)))

	def notice(self, irc, message, by=None):
		if by == None:
			by = irc.config['servid']
		irc._sendraw(":{} NOTICE {} :{}".format(by, self.uid, message[:400]))

	def message(self, irc, message, by=None):
		if by == None:
			by = irc.config['servid']
		irc._sendraw(":{} PRIVMSG {} :{}".format(by, self.uid, message[:400]))

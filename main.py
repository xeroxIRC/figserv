import irc
from time import sleep
import secret

# We get servids 500 to 5ZZ (inclusive) NO ONE TAKE THESE!
config = {
	'host': "127.0.0.1",
	'port': 7829,
	'servname': 'fig.xeroxirc.net',
	'servid': '500',
	'servdesc': "Bigfoots IRC Services",
	'uplinkname': "ga.xeroxirc.net",
	'recvpass': secret.recvpass,
	'sendpass': secret.sendpass
}
irc = irc.IRC(config)
try:
	irc.connect()

	#while True:
	#	print(msg)
	#
	#	if (msg.startswith("CAPAB START")):
	#		sock_send(s, "CAPAB START 1202")
	#		sock_send(s, "CAPAB CAPABILITIES :PROTOCOL=1202")
	#		sock_send(s, "CAPAB END")
	#		sock_send(s, "SERVER custom.xeroxirc.net a 0 420 :Bigfoot's Dab Services")
	#		sock_send(s, "BURST")
	#		sock_send(s, "ENDBURST")
except KeyboardInterrupt:
	irc.stop()
